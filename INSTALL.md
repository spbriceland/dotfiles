## Applications

-   Terminal: hide marking on new prompt lines `[`

    -   Open terminal
    -   Edit > Marks > Uncheck "automatically mark new prompt lines"

-   [Download 1Password](https://1password.com/downloads/mac/)

-   [Install Rectangle for Mac](https://rectangleapp.com/)

    -   Custom Shortcuts
        -   Top Left: `ctrl + opt + {`
        -   Top Right: `ctrl + opt + }`
        -   Bottom Left: `ctrl + opt + ;`
        -   Bottom Right: `ctrl + opt + '`

-   [Install AltTab for Mac](https://alt-tab-macos.netlify.app/)

-   [Install oh my zsh](https://ohmyz.sh/)

-   [Clone dotfiles (this repo)](https://gitlab.com/users/sign_in)

    -   Sign-in, add [ssh key](https://docs.gitlab.com/ee/user/ssh.html) for the new machine, then clone repo onto new machine
    -   Next, copy over shll config files following files into home:

        ```sh
        cp osx/.aliases ~/
        cp osx/.git-completion.zsh ~/
        cp osx/.profile ~/
        cp osx/.zsh_aliases ~/
        cat osx/.zshrc >> ~/.zshrc
        ```

-   [Install Docker Desktop](https://www.docker.com/products/docker-desktop/)

-   [Install Sublime Text](https://www.sublimetext.com/)

-   [Install Node Version Manager](https://github.com/nvm-sh/nvm#installing-and-updating)
    -   Since my shell config files already contains nvm rc scripts, the NVM readme should contain an example command that allows you to install without automatically editing shell config files.
    -   Install a version of node that you wish to be the nvm default. The first node version installed becomes the default. This can be changed later of course.


- [Install VS Code](https://code.visualstudio.com/)

    -   [Setup VS Code for cli usage](https://code.visualstudio.com/docs/setup/mac)
    -   Enable Settings Sync:
        -   Code > Settings > Settings Sync
    -   Reload VS Code to confirm
    - Install all extensions:

        ```sh
        code --install-extension dbaeumer.vscode-eslint
        code --install-extension eamodio.gitlens
        code --install-extension esbenp.prettier-vscode
        code --install-extension redhat.vscode-yaml
        code --install-extension streetsidesoftware.code-spell-checker
        code --install-extension syler.sass-indented
        code --install-extension yoavbls.pretty-ts-errors
        ```
