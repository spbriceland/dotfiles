## SSH Key generation setup

Create ssh key

-   make sure to set a password
-   name it something like: `id_git_key`

```sh
ssh-keygen -t ed25519 -C "spb1014@gmail.com" -f /Users/spb/.ssh/id_git_key -N <password>
```

Check to see if `ssh-agent` is running:

```sh
# if it's running you'll see something like:
# 4694   ??  Ss     0:00.00 ssh-agent
ps x | grep ssh-agent

# confirm PID and SOCK exists
echo $SSH_AGENT_PID
echo $SSH_AUTH_SOCK
```

If it's not running, start the service:

```sh
eval "$(ssh-agent -s)"
```

Open: `~/.ssh/config`

```sh
subl ~/.ssh/config
```

Copy this into the config:

```
# GitLab.com
Host gitlab.com
  AddKeysToAgent yes
  UseKeychain yes
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_git_key

# GitHub
Host github.com
  AddKeysToAgent yes
  UseKeychain yes
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_git_key
```

Add your SSH private key to the ssh-agent and store your passphrase in the keychain

```sh
ssh-add --apple-use-keychain ~/.ssh/id_git_key
```

Copy the public key

```sh
pbcopy < ~/.ssh/id_git_key.pub
```

Then, add it to:

-   [github:](https://github.com/settings/profile) Two keys: one for Authentication and one for Signing
-   [gitlab:](https://gitlab.com/-/profile/keys) One Key: Authentication and signing

## Configure GIT To use and sign key

```sh
git config --global user.name "Sean Briceland"

# This must be github email to work with ssh key
git config --global user.email "seanbriceland@relatecx.com"

# use spb1014@gmail.com when inside of gitlab personal repo
git config user.email "spb1014@gmail.com"


git config --global gpg.format ssh
git config --global user.signingkey ~/.ssh/id_git_key.pub
git config --global commit.gpgsign true
```

## SSH Clean up

Open Keychain Access app
Search for "ssh" and remove any keys

```sh
# remove key from ssh-agent
ssh-add -d path/to/private_key

# Clear out an existing reference known_hosts and config
subl ~/.ssh/known_hosts
subl ~/.ssh/config

# Delete key files
ll ~/.ssh/
rm ~/.ssh/id_
```

## Misc

Killing an `ssh-agent` process:

```sh
eval `ssh-agent -k`
```
