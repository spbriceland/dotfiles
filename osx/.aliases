#!/bin/zsh

# Source shell specific aliases
source ~/.zsh_aliases

# Make some possibly destructive commands more interactive....
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias mkdir='mkdir -pv' 
mcd() { mkdir "$1" && cd "$1"; }

# -- List Fun -- #
alias ll='ls -FGlAhp'
alias la='ls -alF'
alias ls='ls -F'

# -- CD Fun -- #
alias cd..='cd ../'                         # Go back 1 directory level (for fast typers)
alias ..='cd ../'                           # Go back 1 directory level
alias ...='cd ../../'                       # Go back 2 directory levels
alias .3='cd ../../../'                     # Go back 3 directory levels
alias .4='cd ../../../../'                  # Go back 4 directory levels
alias .5='cd ../../../../../'               # Go back 5 directory levels
alias .6='cd ../../../../../../'            # Go back 6 directory levels
alias ~="cd ~"

# -- GIT -- #
alias gadd="git add"
alias gbr="git branch"
alias gbrcontains="git branch --contains"
alias gco="git checkout"
alias gcom="git commit -m"
alias gcomsquash="git commit -m 'm' && squash 2"
alias gdiff="git diff"
alias gds="git diff --staged"
alias gfpull="git fetch && git pull"
glast() { git log -"${1:-1}" --stat; }
alias glo="git log --oneline"
alias grdevelop="git fetch && git rebase origin/develop"
alias grm="git rm -rf"
alias gst="git status"
squash() { git rebase -i head~"$1"; }

current_branch() {
    git branch | grep \* | cut -d ' ' -f2
}

gpushinit() {
    git push origin -u $(current_branch)
}

gcofresh() {
    local branchName=${1:-$(current_branch)} && \
    git fetch && \
    git checkout $branchName && \
    git reset --hard origin/$branchName
}

gco_rebase_develop() {
    gcofresh "$1" && \
    grdevelop
}

gdiffremote() {
    local branchName=${1:-$(current_branch)} && \
    git fetch && \
    git diff origin/$branchName
}

# -- Searching -- #
alias grep='grep --color=auto'              # colorize grep
alias egrep='egrep --color=auto'            # colorize egrep
alias fgrep='fgrep --color=auto'            # colorize fgrep
alias qfind="find . -iname "                # Quickly search for file

# -- Deletion Fun -- #
trash () { mv "$@" ~/.Trash ; }                                 # Move file to trash
alias killDS="find . -type f -name '*.DS_Store' -ls -delete"    # Recursively Delete .DS_Store files
alias killpyc='find . -name \*.pyc -delete'                     # Recusively Delete *.pyc files.

# -- Networking --#
alias myip='curl ip.appspot.com'                    # Public facing IP Address
alias netCons='lsof -i'                             # Show all open TCP/IP sockets
alias flushDNS='dscacheutil -flushcache'            # Flush out the DNS Cache
alias lsock='sudo /usr/sbin/lsof -i -P'             # Display open sockets
alias lsockU='sudo /usr/sbin/lsof -nP | grep UDP'   # Display only open UDP sockets
alias lsockT='sudo /usr/sbin/lsof -nP | grep TCP'   # Display only open TCP sockets
alias ipInfo0='ipconfig getpacket en0'              # Get info on connections for en0
alias ipInfo1='ipconfig getpacket en1'              # Get info on connections for en1
alias openPorts='sudo lsof -i | grep LISTEN'        # All listening connections
alias showBlocked='sudo ipfw list'                  # All ipfw rules inc/ blocked IPs

# -- OS X Spotlight --#
alias indexspotlight='sudo mdutil -E /'

# -- Finder Aliases -- #
alias find.='open -a Finder ./'
alias findshowhidden='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias findhidehidden='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

# -- display all executable paths -- #
alias path='echo -e ${PATH//:/\\n}'

# -- Clear -- #
alias c='clear'

# -- find cpu hogging processes -- #
alias cpu_hogs='ps wwaxr -o pid,stat,%cpu,time,command | head -10'

# Recommended 'top' invocation to minimize resources
alias ttop="top -R -F -s 10 -o rsize"
