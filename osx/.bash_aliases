#!/bin/bash

# -- Bash helpers -- #
alias rlbash='source ~/.bash_profile'           # Reload bash
alias opalias='subl ~/.bash_aliases'            # Open .bash_aliases
alias opbrc='subl ~/.bashrc'                    # Open .bashrc
alias opbprof='subl ~/.bash_profile'            # Open .bash_profile
alias opprof='subl ~/.profile'                  # Open .profile