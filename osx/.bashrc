# Include profile
source ~/.profile

# -- Git Branch Prompt -- #
# Displays git branch name if inside a git project
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# -- function to be ran after each prompt command -- #
#color codes prompt -- Green=Success;Red==Fail
bash_prompt_command() {
    RTN=$?
    prevCmd=$(prevCmd $RTN)
}
PROMPT_COMMAND=bash_prompt_command

# -- Verifies return of previous command -- #
# Returns color based on success of previous command
# Return GREEN if last command was successful
# Return RED if last command Failed
prevCmd() {
    if [ $1 == 0 ] ; then
        echo $GREEN
    else
        echo $RED
    fi
}

# -- set colors using tput if supported -- #
if [ $(tput colors) -gt 0 ] ; then
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    RST=$(tput op)
fi

# -- Prompt String Export -- #
export PS1="\[\$prevCmd\]\u:\[\e[36m\]\w\[\e[0m\]\[\e[38;5;91m\]\$(parse_git_branch)\[\$prevCmd\]>\[$RST\]"

# -- ls color pairs [foreground][background] --#
# BSD LSCOLORS:
# a - black
# b - red
# c - green
# d - brown
# e - blue
# f - pink
# g - cyan
# h - grey
export LSCOLORS="gxfxcxdxbxegedabagacad"

# Include aliases
source ~/.aliases
