# -- Set Default Text Editor -- #
# First, Download & install sublime text, then execute and exoprt EDITOR as sublime
if [ ! -f ~/bin/subl ]; then
    mkdir ~/bin && ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" ~/bin/subl
fi
export PATH=~/bin:$PATH
export EDITOR='subl -w'

# improve zsh pager
export PAGER="less -FRX"

# -- Google Cloud SDK -- #
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/<username>/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/<username>/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/<username>/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/<username>/google-cloud-sdk/completion.zsh.inc'; fi
