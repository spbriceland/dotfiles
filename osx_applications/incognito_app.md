### Create Incognito App via AppleScript Editor

Open AppleScript Editor and paste the following script in it:

```
do shell script "open -a /Applications/Google\\ Chrome.app --args --incognito"
```

Save the script as an application and move it to Launchpad for easier access. If you want to be able to search and run it from Spotlight search, you should add it to the Applications folder.

### Add Logo Image to Incognito App:

Right-click the application you created using AppleScript Editor and select ‘Get Info’. In the window that opens, click the icon at the very top next to the script’s name and hit Ctrl+V. Make sure the padlock icon at the bottom right is unlocked so that the changes take effect. Enter your password if you are prompted to do so.

