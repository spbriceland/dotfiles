### Create Application via AppleScript Editor

Paste the code below into a new AppleScript Editor script and save it as an application

```
on run
    do shell script "open -n /Applications/VLC.app"
    tell application "VLC" to activate
end run

on open theFiles
    repeat with theFile in theFiles
        do shell script "open -na /Applications/VLC.app " & quote & (POSIX path of theFile) & quote
    end repeat
    tell application "VLC" to activate
end open
```

### Associate Video files with newly create VLC Multi app

* Open Finder and find the video file of interest.
* Right click on the file (assumes you have right click enabled).
* Choose "Get Info".
* Under "Open with:", click dropdown and select the VLC droplet/app.
* Click "Change All" button.
* If prompted "are you sure", select "Yes".
