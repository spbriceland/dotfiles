## Install Python Extension
* Search extensions "Python"
* Install Python (ms-python.python via Microsoft)

## Install custom keybindings:
* `cmd + p` to bring up search menu
* enter `> Open Keyboard Shortcuts (JSON)`
* Note: this should be empty. 
* copy and paste contents of keybindings.json here!

## Install user settings
* `cmd + ,` to bring up settings
* Go to "User Settings" tab
* Toggle JSON mode, click `{}`
* Paste `user.settings.json` here
